import React from 'react';
import './App.css';

function Cell(props) {
  return <button className={`cell ${props.className}`} key={props.cellNumber} onClick={props.onClick}>{props.value}</button>
}
function checkWinner(array) {
  let winner = [[0,1,2], [3,4,5], [6,7,8], [0,3,6], [1,4,7], [2,5,8], [0,4,8], [2,4,6]];
  let winnerIndex = winner.findIndex(v => {
    return array[v[0]] && (array[v[0]] === array[v[1]]) &&(array[v[1]] === array[v[2]]);
  });
  let index = winner[winnerIndex];
  return winnerIndex!== -1 ?  {index, player:array[index[0]]} : {index: [], player: null};
}
class Board extends React.Component {
  constructor() {
    super();
    this.state = this.defaultState();
  }
  defaultState() {
    return {
      board: Array(9).fill(null),
      count: 0,
      currentTurn: true,
      winner: { player: null, index: []}
    }
  }
  handleClick(i) {
    if(!this.state.winner.player) {
      const boardCell = this.state.board;
      if(!boardCell[i]) {
        boardCell[i] = this.state.currentTurn ? 'o' : 'x';
        this.setState({currentTurn: !this.state.currentTurn});
        this.setState({board: boardCell});
        this.setState({count: this.state.count + 1});
      }
      const result = checkWinner(this.state.board);
      if(result !== null) {
        this.setState({winner: result})
      }
    }
  }
  onResetClick() {
    this.setState(this.defaultState())
  }
  render() {
    return (
      <React.Fragment>
        <div className="center-h-v mb-20">Current Player {this.state.currentTurn ? 'o' : 'x'}</div>
        <div className="row">
          <Cell className={this.state.winner.index.includes(0) ? 'winner-cell': ''} cellNumber={0} value={this.state.board[0]} onClick={() => this.handleClick(0)}></Cell>
          <Cell className={this.state.winner.index.includes(1) ? 'winner-cell': ''} cellNumber={1} value={this.state.board[1]} onClick={() => this.handleClick(1)}></Cell>
          <Cell className={this.state.winner.index.includes(2) ? 'winner-cell': ''} cellNumber={2} value={this.state.board[2]} onClick={() => this.handleClick(2)}></Cell>
        </div>
        <div className="row">
          <Cell className={this.state.winner.index.includes(3) ? 'winner-cell': ''} cellNumber={3} value={this.state.board[3]} onClick={() => this.handleClick(3)}></Cell>
          <Cell className={this.state.winner.index.includes(4) ? 'winner-cell': ''} cellNumber={4} value={this.state.board[4]} onClick={() => this.handleClick(4)}></Cell>
          <Cell className={this.state.winner.index.includes(5) ? 'winner-cell': ''} cellNumber={5} value={this.state.board[5]} onClick={() => this.handleClick(5)}></Cell>
        </div>
        <div className="row">
          <Cell className={this.state.winner.index.includes(6) ? 'winner-cell': ''} cellNumber={6} value={this.state.board[6]} onClick={() => this.handleClick(6)}></Cell>
          <Cell className={this.state.winner.index.includes(7) ? 'winner-cell': ''} cellNumber={7} value={this.state.board[7]} onClick={() => this.handleClick(7)}></Cell>
          <Cell className={this.state.winner.index.includes(8) ? 'winner-cell': ''} cellNumber={8} value={this.state.board[8]} onClick={() => this.handleClick(8)}></Cell>
        </div>
        <div className="center-h-v mt-20">{this.state.winner.player ? `Winner is ${this.state.winner.player}`: 'Game is on'}</div>
        <div className="center-h-v mt-20">
          {this.state.winner.player ||  this.state.count === 9? <button onClick={() => this.onResetClick()}>Reset</button> : null}
        </div>
      </React.Fragment>
    )
  }

}
function App() {
  return (
    <div className="App">
      <Board />
    </div>
  );
}

export default App;
